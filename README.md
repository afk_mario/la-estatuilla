<div align="center">
<h1>La Estatuilla</h1>

<a href="https://gitlab.com/afk_mario/la-estatuilla">
<img height="100" width="auto" alt="la-estatuilla" src="https://placekitten.com/200/200" />
</a>
<br/>
<p>A digital magazine about the film industry</p>

</div>

<hr />

## Structure

- For changes check  out  the  CHANGELOG
- Use [git commitizenX](https://github.com/streamich/git-cz) for a normalized commit history
